extern crate anyhow;
extern crate thiserror;
mod lexer;
mod parser;
use anyhow::anyhow;
use lexer::{Lexer, TokenType};
use std::path::Path;
use thiserror::Error;
use tokio::{
    self,
    fs::OpenOptions,
    io::{AsyncBufReadExt, BufReader},
    sync::mpsc::{channel, Receiver, Sender},
    task::JoinHandle,
};

#[macro_use]
extern crate lazy_static;

#[derive(Error, Debug)]
enum ProgramError {
    #[error("An unknown error has occurred: {0}")]
    UnknownError(&'static str),
    #[error("{og_error} -- Requested {requested_path}")]
    IOError {
        og_error: std::io::Error,
        requested_path: String,
    },
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let args: Vec<String> = std::env::args().skip(1).collect();

    if args.len() < 1 {
        return Err(anyhow!("No input file specified"));
    }
    let input_file = args.get(0).ok_or(anyhow!("No input file specified"))?;

    let file = OpenOptions::new()
        .read(true)
        .open(Path::new(input_file))
        .await
        .or_else(|e| {
            Err(anyhow!(ProgramError::IOError {
                og_error: e,
                requested_path: input_file.to_owned()
            }))
        })?;

    let mut buf_reader = BufReader::new(file);
    let mut buffer = String::new();

    let (char_buffer_tx, char_buffer_rx): (Sender<Option<String>>, Receiver<Option<String>>) =
        channel(1000);

    let mut lexer = Lexer::new("").with_receiver(char_buffer_rx);

    let lexer_join_handler: JoinHandle<anyhow::Result<()>> = tokio::spawn(async move {
        let mut token = lexer.next_token().await?;
        while token != TokenType::EOF {
            println!("{:?}", token);
            token = lexer.next_token().await?;
        }
        println!("{:?}", token);

        Ok(())
    });

    while let Ok(read_bytes) = buf_reader.read_line(&mut buffer).await {
        if read_bytes < 1 {
            break;
        }
        char_buffer_tx
            .send(Some(buffer.clone()))
            .await
            .map_err(|_| anyhow!(ProgramError::UnknownError("Unable to send data to lexer")))?;
        buffer.clear();
    }

    // Send finished message
    char_buffer_tx
        .send(None)
        .await
        .map_err(|_| anyhow!(ProgramError::UnknownError("Unable to send data to lexer")))?;

    lexer_join_handler
        .await
        .map_err(|_| anyhow!("Lexer thread could not be joined to main thread"))??;

    return Ok(());
}
