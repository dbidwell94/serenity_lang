use super::{
    Keyword, Operator, Punctuation, PROGRAM_KEYWORDS, PROGRAM_OPERATOR, PROGRAM_PUNCTUATION,
};
use anyhow::anyhow;
use lazy_static;
use regex::Regex;
use std::collections::VecDeque;
use thiserror::Error;

lazy_static! {
    static ref NUMBER_REGEX: Regex = Regex::new(r"^[0-9]{0,}([.][0-9]{1,})?$").unwrap();
    static ref IDENTIFIER_REGEX: Regex = Regex::new(r"^[a-zA-Z\$]\w*$").unwrap();
}

#[derive(Debug, PartialEq, Clone)]
pub enum TokenType {
    Punctuation(Punctuation),
    TypeIdentifier(String),
    Identifier(String),
    Operator(Operator),
    Keyword(Keyword),
    String(String),
    Number(f64),
    EOF,
}

#[derive(Error, Debug)]
pub enum LexerError {
    #[error("An unknown symbol was detected: {symbol:?}")]
    UnknownSymbol { symbol: String },
    #[error("Recieved an unexpected symbol. Expected {expected:?}, recieved {symbol:?}")]
    UnexpectedSymbol { symbol: String, expected: TokenType },
    #[error("Unexpected end of token stream: {0}")]
    UnexpectedEndOfStream(String),
    #[error("An unexpected token stream error has occurred")]
    TokenStreamError,
    #[error("Unable to parse {to_parse:?} into {parse_into:?}")]
    ParseError {
        to_parse: String,
        parse_into: String,
    },
}

pub struct Lexer {
    chars: VecDeque<char>,
    sqr_bracket_count: i64,
    angle_bracket_count: i64,
    bracket_count: i64,
    paren_count: i64,
    double_quote: bool,
    single_quote: bool,
    literal_quote: bool,
    is_reading_in_quote: bool,
    is_reading_number: bool,
    is_reading_identifier: bool,
    is_reading_type_identifier: bool,
    current_column: usize,
    current_line: usize,
    receiver: Option<tokio::sync::mpsc::Receiver<Option<String>>>,
}

impl Lexer {
    pub fn new(source_input: &str) -> Lexer {
        Lexer {
            chars: source_input.chars().collect(),
            bracket_count: 0,
            paren_count: 0,
            sqr_bracket_count: 0,
            angle_bracket_count: 0,
            is_reading_in_quote: false,
            is_reading_number: false,
            double_quote: false,
            literal_quote: false,
            is_reading_identifier: false,
            is_reading_type_identifier: false,
            single_quote: false,
            current_column: 1,
            current_line: 1,
            receiver: None,
        }
    }

    pub fn with_receiver(mut self, receiver: tokio::sync::mpsc::Receiver<Option<String>>) -> Lexer {
        self.receiver = Some(receiver);
        return self;
    }

    pub fn append_chars(&mut self, source_input: &str) {
        let mut new_chars: VecDeque<char> = source_input.chars().collect();
        self.chars.append(&mut new_chars);
    }

    fn read_string_from_chars(&mut self, check_for_quotes: bool) -> anyhow::Result<String> {
        let mut str_buffer: String = String::new();
        while self.is_reading_in_quote
            || self.is_reading_identifier
            || self.is_reading_type_identifier
        {
            let next_char = self.chars.front();

            if check_for_quotes {
                let next_char_unwrapped = next_char.ok_or(anyhow!(
                    LexerError::UnexpectedEndOfStream(String::from("String was never terminated"))
                ))?;
                if (self.double_quote && next_char_unwrapped == &'\"')
                    || (self.single_quote && next_char_unwrapped == &'\'')
                    || (self.literal_quote && next_char_unwrapped == &'`')
                {
                    self.is_reading_in_quote = false;
                    return Ok(str_buffer);
                }
            } else {
                if let Some(next_char_unwrapped) = next_char {
                    if PROGRAM_PUNCTUATION.contains_key(next_char_unwrapped)
                        || next_char_unwrapped.is_whitespace()
                        || PROGRAM_OPERATOR
                            .contains_key(&String::from(next_char_unwrapped.to_owned()))
                    {
                        self.is_reading_identifier = false;
                        return Ok(str_buffer);
                    }
                } else {
                    self.is_reading_identifier = false;
                    return Ok(str_buffer);
                }
            }
            str_buffer.push(self.chars.pop_front().ok_or(anyhow!(
                LexerError::UnexpectedEndOfStream(String::from(
                    "chars buffer not expected to be empty (read_string_from_chars)"
                ))
            ))?);
        }

        return Err(anyhow!(str_buffer));
    }

    fn read_identifier_from_chars(&mut self) -> anyhow::Result<TokenType> {
        let identifier = self.read_string_from_chars(false)?;
        if let Some(keyword) = PROGRAM_KEYWORDS.get(&identifier) {
            match keyword {
                Keyword::Await => return Ok(TokenType::Keyword(keyword.to_owned())),
                _ => {
                    return Err(anyhow!(LexerError::UnexpectedSymbol {
                        symbol: identifier,
                        expected: TokenType::String(String::from("Any"))
                    }))
                }
            }
        }
        return Ok(TokenType::Identifier(identifier));
    }

    fn read_number_from_chars(&mut self, str_buffer: &mut String) -> anyhow::Result<TokenType> {
        while self.is_reading_number {
            let next_char = self.chars.front();
            if let Some(unwrapped_next_char) = next_char {
                let mut test_buffer = str_buffer.to_owned();
                test_buffer.push(unwrapped_next_char.to_owned());
                // Keep collecting chars into the str buffer to finish the number
                if NUMBER_REGEX.is_match(&test_buffer) {
                    str_buffer.push(self.chars.pop_front().ok_or(anyhow!(
                        LexerError::UnexpectedEndOfStream(String::from(
                            "char buffer not expected to be empty"
                        ))
                    ))?);
                    continue;
                } else {
                    self.is_reading_number = false;
                    return Ok(TokenType::Number(str_buffer.parse::<f64>().or_else(
                        |_| {
                            return Err(anyhow!(LexerError::ParseError {
                                to_parse: str_buffer.to_owned(),
                                parse_into: String::from("Number")
                            }));
                        },
                    )?));
                }
            } else {
                self.is_reading_number = false;
                return Ok(TokenType::Number(str_buffer.parse::<f64>().or_else(
                    |_| {
                        return Err(anyhow!(LexerError::ParseError {
                            to_parse: str_buffer.to_owned(),
                            parse_into: String::from("Number")
                        }));
                    },
                )?));
            }
        }
        return Err(anyhow!(LexerError::TokenStreamError));
    }

    pub async fn next_token(&mut self) -> anyhow::Result<TokenType> {
        let mut str_buffer = String::new();
        let mut punctuation_char: Option<char> = None;

        if self.chars.len() < 1 {
            self.fill_buffer_from_rx().await;
        }

        while self.chars.len() > 0 {
            if !self.is_reading_in_quote || !self.is_reading_number {
                self.trim_whitespace();
                if self.chars.len() < 1 {
                    self.fill_buffer_from_rx().await;
                }
            }

            // String
            if self.is_reading_in_quote {
                return Ok(TokenType::String(self.read_string_from_chars(true)?));
            }

            // Identifier
            if self.is_reading_identifier {
                return self.read_identifier_from_chars();
            }

            if self.chars.len() < 1 {
                break;
            }

            str_buffer.push(self.chars.pop_front().ok_or(anyhow!(
                LexerError::UnexpectedEndOfStream(String::from(
                    "chars buffer not expected to be empty"
                ))
            ))?);

            // Number
            if NUMBER_REGEX.is_match(&str_buffer) {
                self.is_reading_number = true;
                return self.read_number_from_chars(&mut str_buffer);
            }

            if punctuation_char.is_none() && str_buffer.len() == 1 {
                punctuation_char = Some(
                    str_buffer
                        .chars()
                        .nth(0)
                        .ok_or(anyhow!(LexerError::TokenStreamError))?,
                );
            }

            // Punctuation
            if let Some(unwrapped_punctuation_char) = punctuation_char {
                if let Some(punctuation_token) =
                    PROGRAM_PUNCTUATION.get(&unwrapped_punctuation_char)
                {
                    // Test to make sure cases like >> and << don't get caught as punctuation
                    if let Some(next_punctuation_char) = self.chars.front() {
                        if next_punctuation_char == &unwrapped_punctuation_char {
                            continue;
                        }
                    }
                    if str_buffer.len() == 1 {
                        match punctuation_token {
                            Punctuation::DoubleQuote => {
                                self.double_quote = !self.double_quote;
                            }
                            Punctuation::SingleQuote => {
                                self.single_quote = !self.single_quote;
                            }
                            Punctuation::BackTick => {
                                self.literal_quote = !self.literal_quote;
                            }
                            Punctuation::AngleBracketLeft => {
                                self.angle_bracket_count += 1;
                            }
                            Punctuation::AngleBracketRight => {
                                self.angle_bracket_count -= 1;
                                if self.angle_bracket_count < 0 {
                                    return Err(anyhow!(LexerError::UnexpectedSymbol {
                                        symbol: String::from(unwrapped_punctuation_char),
                                        expected: TokenType::Punctuation(
                                            Punctuation::AngleBracketLeft
                                        )
                                    }));
                                }
                            }
                            Punctuation::LeftBracket => {
                                self.bracket_count += 1;
                            }
                            Punctuation::RightBracket => {
                                self.bracket_count -= 1;
                                if self.bracket_count < 0 {
                                    return Err(anyhow!(LexerError::UnexpectedSymbol {
                                        symbol: String::from(unwrapped_punctuation_char),
                                        expected: TokenType::Punctuation(Punctuation::LeftBracket)
                                    }));
                                }
                            }
                            Punctuation::LeftParen => {
                                self.paren_count += 1;
                            }
                            Punctuation::RightParen => {
                                self.paren_count -= 1;
                                if self.paren_count < 0 {
                                    return Err(anyhow!(LexerError::UnexpectedSymbol {
                                        symbol: String::from(unwrapped_punctuation_char),
                                        expected: TokenType::Punctuation(Punctuation::LeftParen)
                                    }));
                                }
                            }
                            Punctuation::LeftSqrBracket => {
                                self.sqr_bracket_count += 1;
                            }
                            Punctuation::RightSqrBracket => {
                                self.sqr_bracket_count -= 1;
                                if self.sqr_bracket_count < 0 {
                                    return Err(anyhow!(LexerError::UnexpectedSymbol {
                                        symbol: String::from(unwrapped_punctuation_char),
                                        expected: TokenType::Punctuation(
                                            Punctuation::LeftSqrBracket
                                        )
                                    }));
                                }
                            }
                            Punctuation::Period => {
                                if let Some(next_punctuation_char) = self.chars.front() {
                                    if NUMBER_REGEX
                                        .is_match(&String::from(next_punctuation_char.to_owned()))
                                    {
                                        self.is_reading_number = true;
                                        continue;
                                    }
                                }
                            }
                            _ => {}
                        };
                        if self.double_quote || self.single_quote || self.literal_quote {
                            self.is_reading_in_quote = true;
                        } else {
                            self.is_reading_in_quote = false;
                        }

                        return Ok(TokenType::Punctuation(punctuation_token.to_owned()));
                    }
                }
            }

            // Operator
            if let Some(operator_token) = PROGRAM_OPERATOR.get(&str_buffer) {
                self.trim_whitespace();
                if let Some(next_char) = self.chars.front() {
                    if IDENTIFIER_REGEX.is_match(&String::from(next_char.to_owned())) {
                        self.is_reading_identifier = true;
                    }
                }
                return Ok(TokenType::Operator(operator_token.to_owned()));
            }

            // Keyword
            if let Some(keyword_token) = PROGRAM_KEYWORDS.get(&str_buffer) {
                match keyword_token {
                    Keyword::Fn | Keyword::Let => {
                        self.is_reading_identifier = true;
                        self.trim_whitespace();
                    }
                    _ => {}
                };
                return Ok(TokenType::Keyword(keyword_token.to_owned()));
            }

            // Check for function invocations and indexers like main() or vec[1]
            if let Some(next_char) = self.chars.front() &&
                let Some(punctuation) = PROGRAM_PUNCTUATION.get(next_char) &&
                !str_buffer.is_empty() {
                match punctuation {
                    Punctuation::LeftSqrBracket |
                    Punctuation::LeftParen |
                    Punctuation::RightParen |
                    Punctuation::Comma |
                    Punctuation::RightSqrBracket |
                    Punctuation::SemiColon |
                    Punctuation::Period => {
                        return Ok(TokenType::Identifier(str_buffer.to_owned()));
                    },
                    _ => continue
                }
            }

            // Check the receiver to ensure no waiting chars
            if self.chars.len() < 1 {
                self.fill_buffer_from_rx().await;
            }
        }
        if !str_buffer.is_empty() {
            return Err(anyhow!(LexerError::UnknownSymbol { symbol: str_buffer }));
        }
        return Ok(TokenType::EOF);
    }

    async fn fill_buffer_from_rx(&mut self) {
        while self.chars.len() < 1 {
            if let Some(receiver) = &mut self.receiver {
                if let Some(str) = receiver.recv().await {
                    if str.is_none() {
                        self.receiver = None;
                        break;
                    } else {
                        self.append_chars(&str.unwrap());
                    }
                    self.trim_whitespace();
                } else {
                    break;
                }
            } else {
                break;
            }
        }
    }

    fn trim_whitespace(&mut self) {
        while self.chars.len() > 0 {
            if let Some(character) = self.chars.front() {
                if !character.is_whitespace() {
                    break;
                } else {
                    if character == &'\n' {
                        self.current_line += 1;
                        self.current_column = 1;
                    } else {
                        self.current_column += 1;
                    }
                    self.chars.pop_front();
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::lexer::{Keyword, Operator, Punctuation};

    use super::{Lexer, TokenType};

    #[tokio::test]
    async fn test_lexer_punctuation() {
        let mut lexer = Lexer::new("[]()");

        assert_eq!(
            TokenType::Punctuation(Punctuation::LeftSqrBracket),
            lexer.next_token().await.unwrap()
        );
        assert_eq!(
            TokenType::Punctuation(Punctuation::RightSqrBracket),
            lexer.next_token().await.unwrap()
        );
        assert_eq!(
            TokenType::Punctuation(Punctuation::LeftParen),
            lexer.next_token().await.unwrap()
        );
        assert_eq!(
            TokenType::Punctuation(Punctuation::RightParen),
            lexer.next_token().await.unwrap()
        );
        assert_eq!(TokenType::EOF, lexer.next_token().await.unwrap())
    }

    #[tokio::test]
    async fn test_lexer_punctuation_error() {
        let mut lexer = Lexer::new(">");
        assert_eq!(true, lexer.next_token().await.is_err());
        lexer = Lexer::new("}");
        assert_eq!(true, lexer.next_token().await.is_err());
        lexer = Lexer::new("]");
        assert_eq!(true, lexer.next_token().await.is_err());
        lexer = Lexer::new(")");
        assert_eq!(true, lexer.next_token().await.is_err());
        lexer = Lexer::new("()[]{}<>");

        let mut token = lexer.next_token().await.unwrap();
        while token != TokenType::EOF {
            let next_token = lexer.next_token().await.unwrap();
            token = next_token;
        }
    }

    #[tokio::test]
    async fn test_lexer_remove_whitespace() {
        let mut lexer = Lexer::new("  (  );");

        assert_eq!(
            TokenType::Punctuation(Punctuation::LeftParen),
            lexer.next_token().await.unwrap()
        );
        assert_eq!(
            TokenType::Punctuation(Punctuation::RightParen),
            lexer.next_token().await.unwrap()
        );
        assert_eq!(
            TokenType::Punctuation(Punctuation::SemiColon),
            lexer.next_token().await.unwrap()
        )
    }

    #[tokio::test]
    async fn test_lexer_operator() {
        let mut lexer = Lexer::new("+ - & | >> <<");
        assert_eq!(
            TokenType::Operator(Operator::Plus),
            lexer.next_token().await.unwrap()
        );
        assert_eq!(
            TokenType::Operator(Operator::Minus),
            lexer.next_token().await.unwrap()
        );
        assert_eq!(
            TokenType::Operator(Operator::And),
            lexer.next_token().await.unwrap()
        );
        assert_eq!(
            TokenType::Operator(Operator::Or),
            lexer.next_token().await.unwrap()
        );
        assert_eq!(
            TokenType::Operator(Operator::ShiftRight),
            lexer.next_token().await.unwrap()
        );
        assert_eq!(
            TokenType::Operator(Operator::ShiftLeft),
            lexer.next_token().await.unwrap()
        );
        assert_eq!(TokenType::EOF, lexer.next_token().await.unwrap());
    }

    #[tokio::test]
    async fn test_lexer_keywords() {
        let mut lexer = Lexer::new("let");

        assert_eq!(
            TokenType::Keyword(Keyword::Let),
            lexer.next_token().await.unwrap()
        );
        lexer = Lexer::new("export");
        assert_eq!(
            TokenType::Keyword(Keyword::Export),
            lexer.next_token().await.unwrap()
        );
        lexer = Lexer::new("import");
        assert_eq!(
            TokenType::Keyword(Keyword::Import),
            lexer.next_token().await.unwrap()
        );
        lexer = Lexer::new("if");
        assert_eq!(
            TokenType::Keyword(Keyword::If),
            lexer.next_token().await.unwrap()
        );
        lexer = Lexer::new("for");
        assert_eq!(
            TokenType::Keyword(Keyword::For),
            lexer.next_token().await.unwrap()
        );
        lexer = Lexer::new("while");
        assert_eq!(
            TokenType::Keyword(Keyword::While),
            lexer.next_token().await.unwrap()
        );
        lexer = Lexer::new("fn");
        assert_eq!(
            TokenType::Keyword(Keyword::Fn),
            lexer.next_token().await.unwrap()
        );
        assert_eq!(TokenType::EOF, lexer.next_token().await.unwrap());
    }

    #[tokio::test]
    async fn test_double_quote_recognition() {
        let mut lexer = Lexer::new("\"This is a test\"");
        assert_eq!(
            TokenType::Punctuation(Punctuation::DoubleQuote),
            lexer.next_token().await.unwrap()
        );
        assert_eq!(
            TokenType::String(String::from("This is a test")),
            lexer.next_token().await.unwrap()
        );
        assert_eq!(
            TokenType::Punctuation(Punctuation::DoubleQuote),
            lexer.next_token().await.unwrap()
        );
    }

    #[tokio::test]
    async fn test_single_quote_recognition() {
        let mut lexer = Lexer::new("'This is a test'");
        assert_eq!(
            TokenType::Punctuation(Punctuation::SingleQuote),
            lexer.next_token().await.unwrap()
        );
        assert_eq!(
            TokenType::String(String::from("This is a test")),
            lexer.next_token().await.unwrap()
        );
        assert_eq!(
            TokenType::Punctuation(Punctuation::SingleQuote),
            lexer.next_token().await.unwrap()
        );
    }

    #[tokio::test]
    async fn test_literal_quote_recognition() {
        let mut lexer = Lexer::new("`This is a test`");
        assert_eq!(
            TokenType::Punctuation(Punctuation::BackTick),
            lexer.next_token().await.unwrap()
        );
        assert_eq!(
            TokenType::String(String::from("This is a test")),
            lexer.next_token().await.unwrap()
        );
        assert_eq!(
            TokenType::Punctuation(Punctuation::BackTick),
            lexer.next_token().await.unwrap()
        );
    }

    #[tokio::test]
    async fn test_bad_string() {
        let mut lexer = Lexer::new("'Bad string");
        lexer.next_token().await.unwrap();
        assert_eq!(true, lexer.next_token().await.is_err());
    }

    #[tokio::test]
    async fn test_number_recognition() {
        let mut lexer = Lexer::new("876");
        assert_eq!(TokenType::Number(876f64), lexer.next_token().await.unwrap());
        lexer.append_chars(".34");
        assert_eq!(TokenType::Number(0.34), lexer.next_token().await.unwrap());
        lexer.append_chars("(.65, .75)");
        lexer.next_token().await.unwrap();
        assert_eq!(TokenType::Number(0.65), lexer.next_token().await.unwrap());
        lexer.next_token().await.unwrap();
        assert_eq!(TokenType::Number(0.75), lexer.next_token().await.unwrap());
    }

    #[tokio::test]
    async fn full_test_source() {
        let mut lexer = Lexer::new(
            r"fn test() {
            let i = 0;
        }",
        );
        let expected_vec: Vec<TokenType> = vec![
            TokenType::Keyword(Keyword::Fn),
            TokenType::Identifier(String::from("test")),
            TokenType::Punctuation(Punctuation::LeftParen),
            TokenType::Punctuation(Punctuation::RightParen),
            TokenType::Punctuation(Punctuation::LeftBracket),
            TokenType::Keyword(Keyword::Let),
            TokenType::Identifier(String::from("i")),
            TokenType::Operator(Operator::Equals),
            TokenType::Number(0.0),
            TokenType::Punctuation(Punctuation::SemiColon),
            TokenType::Punctuation(Punctuation::RightBracket),
            TokenType::EOF,
        ];
        for token in expected_vec {
            assert_eq!(token, lexer.next_token().await.unwrap());
        }
    }
}
