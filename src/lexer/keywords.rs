use phf::{phf_map, Map};

#[derive(Debug, PartialEq, Clone)]
pub enum Keyword {
    If,
    While,
    For,
    Import,
    Export,
    Fn,
    Let,
    Async,
    Await,
    Return
}

pub static PROGRAM_KEYWORDS: Map<&'static str, Keyword> = phf_map! {
    "if" => Keyword::If,
    "while" => Keyword::While,
    "for" => Keyword::For,
    "import" => Keyword::Import,
    "export" => Keyword::Export,
    "fn" => Keyword::Fn,
    "let" => Keyword::Let,
    "async" => Keyword::Async,
    "await" => Keyword::Await,
    "return" => Keyword::Return
};
