use phf::{phf_map, Map};

#[derive(Debug, Clone, PartialEq)]
pub enum Punctuation {
    Period,
    Comma,
    SemiColon,
    Colon,
    RightBracket,
    LeftBracket,
    RightSqrBracket,
    LeftSqrBracket,
    RightParen,
    LeftParen,
    AngleBracketRight,
    AngleBracketLeft,
    DoubleQuote,
    SingleQuote,
    BackTick,
    Bang
}

pub static PROGRAM_PUNCTUATION: Map<char, Punctuation> = phf_map! {
    '.' => Punctuation::Period,
    ',' => Punctuation::Comma,
    ';' => Punctuation::SemiColon,
    ':' => Punctuation::Colon,
    '}' => Punctuation::RightBracket,
    '{' => Punctuation::LeftBracket,
    ']' => Punctuation::RightSqrBracket,
    '[' => Punctuation::LeftSqrBracket,
    ')' => Punctuation::RightParen,
    '(' => Punctuation::LeftParen,
    '>' => Punctuation::AngleBracketRight,
    '<' => Punctuation::AngleBracketLeft,
    '\'' => Punctuation::SingleQuote,
    '"' => Punctuation::DoubleQuote,
    '`' => Punctuation::BackTick,
    '!' => Punctuation::Bang
};
