use phf::{phf_map, Map};

#[derive(Debug, Clone, PartialEq)]
pub enum Operator {
    Plus,
    Minus,
    Multiply,
    Divide,
    Equals,
    ShiftLeft,
    ShiftRight,
    And,
    Or,
    LogicalAnd,
    LocicalOr,
}

pub static PROGRAM_OPERATOR: Map<&'static str, Operator> = phf_map! {
    "+" => Operator::Plus,
    "-" => Operator::Minus,
    "*" => Operator::Multiply,
    "/" => Operator::Divide,
    "=" => Operator::Equals,
    "<<" => Operator::ShiftLeft,
    ">>" => Operator::ShiftRight,
    "&" => Operator::And,
    "|" => Operator::Or,
    "&&" => Operator::LogicalAnd,
    "||" => Operator::LocicalOr
};
